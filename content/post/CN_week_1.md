---
title: "CN_week_1"
date: 2017-10-22T10:00:58+02:00
draft: false
---

#CN week 1

## HTML et CSS

Cette semaine on à commencé avec du HTML5 CSS3.     
Projet 1: faire sont CV en html css.     
Du coup j'en ai profité pour faire quelques révision et expérimenter
les flexboxs. Le site du W3School reste quand même une référence en la matière je trouve.
J'ai aussi testé les animation en css. Au final c'est pas bien compliqué pour des animes ultra basiques.
J'ai enfin éclaircie un truc sur les balise orphelines, on met bien un slash à la fin. c'est pas indispensable
mais c'est mieux et au moins je me poserai plus la question. Donc on à bien :
```
<br/>
<hr />
<img src="assets/monimage.png" />

```

## Photoshop

On a aussi fait du photoshop, là c'était vraiment découverte de l'outils.
Mais il y'a un truc dont je me servais très rarement et qui est une bonne pratique,
c'est de se servir des masques de calque et des réglages de calque directement dans la pile, 
afin de travailler de manière non destructive. C'est vrai que j'avais quand même l'habithude de bosser comme un bourrin :(.
