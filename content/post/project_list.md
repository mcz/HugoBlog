---
title: "Project_list"
date: 2018-10-01T01:18:26+02:00
draft: true
---
# list_of_things_to_learn
An idea list of things to learn and practice during the next 6 months
- [ ] REACT && || VUE (JS FRAMEWORK)
- [ ] PYTHON FLASK && || DJANGO
- [ ] GO LANG
- [ ] WEB 3D
- [ ] GIS
- [ ] CREATIVE CODING (processing P5)
- [ ] UNREAL ENGINE / BLENDER -> BLEND4WEB
- [ ] TDD JS/PYTHON
- [ ] COCO'S 2D
- [ ] SHADERS

- [ ] Create an interactive map of street pinpon table
- [ ] Create an interactive map of spatial observatorys
- [ ] Create an interactive map of plane / boat circulation
- [ ] A web app for generate and create the Game "who is?"
- [ ] A game with QR codes in the city
- [ ] ...