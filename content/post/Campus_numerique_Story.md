---
title: "Campus_numerique_Story"
date: 2017-10-12T12:18:26+02:00
draft: false
---
# QuesCeQueCestQueCa ?
Voilà c'est officiel j'ai était admis à Digital campus Grenoble ou le campus numérique in the Alps.   
J'ai l'intention d'écrire des articles à ce sujet car quand on cherche une formation on ne sait jamais comment ça marche de l'intérieur.
Cela m'a parfois fait perdre pas mal de temps à trainer sur la toile et il faut bien l'avouer j'ai très rarement trouvé des infos sur 
le pourquoi du comment.


## Présentation rapide de DG (digital campus grenoble)
Il s'agit d'une formation pour être analiste dévellopeur informatique.
Equivalent à BAC+2.
Pour intégrer cette formation il faut être demandeur d'emploi.
La formation dure 18 mois : 6 mois de cours (intensif) puis 12 mois en alternance.


## 1- Candidater (juin)
il faut
une lettre de motivation
un CV
et un pitch vidéo de 1'30min

Le pitch vidéo à été pour moi un exercice un peu difficile.
Divers problemes techniques(bruits des ventilos, format vidéo, cadrage)
j'ai fait vraiment plein d'enregistrement (presque 100 ... Oui Monsieur)
Bon l'avantage c'est que mon texte je le connaissais parfaitement le rythme
l'intonation. Tout ça pour dire que ce n'est pas forcement un exercice facile et qu'il vaut mieux bien le préparer.
Après il y'aura toujours des gens qui l'auront fait en une prise avec un flow parfait et naturel.

## 2- Les tests (juillet)
Après avoir envoyer tout ça, on reçois une convocation pour passer les tests praditus.
ça dure 1 heure il me semble. On arrive dans une salle avec d'autre prétendants on se met sur un poste informatique
avec un windows 7 dessus et on se connecte sur une adresse web puis remplir un formulaire (nom, prénom ...).
Une serie de questions psychologique du genre "Si un collègue déprime":
a- vous l'ignorer
b- vous lui demander ce qui ne va pas
c- vous lui apporter un café
etc....
En gros ça sert à rien de mentir sur ta personnalité ça se verra dans la suite du recrutement.
Donc en gros tu répond comme tu le sens.
Ils placent aussi une petite section de test psychotechnique (10 min env), des suite de nombres et patata.

## 3- La piscine (septembre)
Alors un mois après tu reçois ta convocation à la piscine. Et ça c'est la partie marrante du processus de selection.
Pour cette promo il y avait 60 personnes préselectionné (pas tout le monde n'est convié; un pote n'a pas eu de convoc fin de l'histoire),
la premiere semaine 30 sont venu (dont moi) la on te ditil faut faire 2 équipe de 15 pioché au hazard. Puis chaque équipe doit aller dans une salle.
Là on nous dit :"Vous avez une semaine pour réaliser la commande d'un prototype pour un client". on avais à disposition plein de truc : tubes, papiers, carton paille dominos, scotch, billes, balle de ping-pong, ficelle, lego.
Et la commande c'était une machine infernale avec quelques contrainte comme : le parcours doit faire au moins 19m, il doit y avoir un moulin, un levier...
Tu te retrouve avec 14 personne de 22 à 45 ans que t'as jamais vu et tu dois faire ça. Et tous les jours vers 15h00 quelqu'un de DC arrive pour annoncer des nouvelles contraintes du client, classic, comme dans la vrai vie quoi. Pour le coup l'équipe avec qui j'était on tous était des gens super,
ça c'est bien passé et plutôt naturellement. Donc on a fait cette machine infernale et c'etait une expérience social vraiment sympa. Ah oui pendant que tu bosses avec tes collègues des gens viennent avec leur ordinateur et t'observe c'est space au départ mais t'es tellement dans le jus que tu fait plus attention.
Parfois de potentielles entreprise passe se présenter (il faudra en choisir par la suite), On en à eu 8 je crois.

## 4- Les entretients
2 semaines plus tard on te file la liste de toutes les entreprises prêtent à prendre un alternant (Certaine sont passé se présenter pendant la piscine).
Il faut en choisir 10, 5 que tu veux absolument voir et 5 optionnelles. Ben là c'est déjà un peu le casse tête. Tout commence à être stratégique, car il 
faut en effet savoir que si tu n'est pas pris dans une de ces boites tu n'as pas la formation. et il y'a un peu moins de 30 ets et les bruits de couloir 
m'ont dit qu'il restait 45 persnnes après la piscine. Donc 15 d'entre nous vont sauter. J'avais pas misé sur les grosses boite (SSII) leur aura fait que trop de monde voulais y aller cela réduisait mes chances, en plus je viens des pme, les trucs plus humains, alors l'usine du dev c'est pas trop mon truc.
Puis la semaine d'après on t'envoi les heures de passage pour les entretients durant la semaine. C'est des speed jobing c.a.d 7 min d'entretient chrono.
Et là faut être bon parce ce que ils en voit 20 passer dans la 1/2 journée et tu sais pas à qui tu as à faire. Bon après 3 entretient tondiscours commence à être au point fléxible et controlé. Et puis y'a des boite que tu sentais pas spécialement qui on en fait d'avoir l'air superbe (il faut bien choisir quand même, tu reste au moins 1 an) et l'inverse est vrai aussi.
C'est une semaine plutôt épuisante, t'es en stress comme un entretien tu changes ton jeu et tu te dit merde si je suis pas le favoris d'une de ces boîte c'est mort. En m^me temps t'as tout donné et c'est hyper formateur et tu rencontre des boites que autrement tu n'aurais jamais pu approcher.

## 5- Le matching
Ensuite on te demande de faire ta liste de boite, six au total qui te plairais d'intégrer.
C'est pareil pour les entreprises elle listent 6 personnes qu'elle seraient prêtent à intégrer.
le tout de 1 à 6.
Et là grosse réflexion, avec qui ça à eu l'air de coller, chez qui ça le ferait pas vraiment de bosser pendant au moins 1 an.
Es ce que cette boîte peux m'apporter des compétences et un peu de suivis ou bien il vont me mettre dans un projet à la noix qui comptent pas vraiment.
Es ce qu'ils sont loin?(bon ça fallait le penser avant mon coco).

Et voilà moi ça as marché la boite placé en N°1 :)

Tout ça pour dire que le processus peut paraitre bizzard mais il eest formateur.
Il est donc préférable de se préparer. mes conseils pour ce qu'ils valent.
1- un bon CV pas à l'arrache mais si t'es gaphiste ça sert à rien de faire une oeuvre tout le monde s'en fout.
2- la lettre de motive est un élémnt clé
3- le pitch vidéo faut s'y prendre un peu à l'avance car on se rend pas vraiment compte tant qu'on l'a pas fait 
je pensé qu'en 4-5 prises se serait plié, beh pas du tout 100 oui monsieur!

4-réviser les test psychotechniques il y'a des livres "it worst the price"
5- Pour la piscine rien de spécial, faut être à l'heure et ça dépend du projet des gens faut savoir être bien en société.
6- les entretients c'est bien d'être au point sur son spitch et d'apporter votre CV plus book

Sinon c'est bien d'avoir fait quelques mooc sur des languages (HTML / CSS / JS python PHP....) car ça permet de montrer au recruteur que l'on sait à quoi
ça engage d'être dev que l'on à dejà quelques réalisation fonctionnels. Je sais pas mais si tu fait un jeu sur ton téléphone tu peux sortir une phrase du genre:
durant le moi d'Aout j'ai bosser sur un jeu en JAVA pour mon android voilà comment ça marche"
Et puis rentrer dans une formation dev sans avoir entendu le mot variable ou fonction ça doit être chaud quand même.



