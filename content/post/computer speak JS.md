---
title: "Computer speak JS"
date: 2017-09-21T13:48:02+02:00
draft: false
---

# Make the browser speak again
```
let synth = window.speechSynthesis;
let utterThis = new SpeechSynthesisUtterance('Hello jojo');
synth.speak(utterThis);
```
